const roomName = JSON.parse(document.getElementById('json-roomname').textContent);
const userName = JSON.parse(document.getElementById('json-username').textContent);
const chatSocket = new WebSocket(
    'ws://'
    + window.location.host
    + '/ws/'
    + roomName
    + '/'
    + 'chat/'
);

chatSocket.onclose = function (e) {
    console.log('onclose')
}

chatSocket.onmessage = function (e) {
    const data = JSON.parse(e.data);
    let dateObj = new Date();
    let month = dateObj.getUTCMonth() + 1; //months from 1-12
    let day = dateObj.getUTCDate();
    let year = dateObj.getUTCFullYear();
    let newdate = day + "/" + month + "/" + year;
    if (data.message) {
        let msg_render =
            ("<div class=\"d-flex flex-column rounded bg-secondary bg-opacity-10 mt-3 mb-2 pb-1 msg\">" +
                "<div class=\"d-flex justify-content-center align-items-center p-3 mt-2\">" +
                "<img src=\"\\static\\img\\avatar.jpg\" class=\"rounded-circle me-4 msg-img\" alt=\"msg-pic\">" +
                "<h5 class=\"f-sm-bold text-uppercase\">" + data.username + "</h5>" +
                "</div>" +
                "<div class=\"text-center text-wrap msg-content mt-2\">" +
                "<p class=\"f-slim\">" +
                data.message +
                "</p>" +
                "</div>" +
                "<p class=\"f-bold c-grey msg-date text-end me-3\">" +
                newdate +
                "</p>" +
                "</div>");
        document.querySelector('#chat-messages').innerHTML += msg_render;

        scrollToBottom();
    } else {
        alert('The message was empty!')
    }
};

document.querySelector('#chat-message-input').focus();

document.querySelector('#chat-message-input').onkeyup = function (e) {
    if (e.keyCode === 13) {
        document.querySelector('#chat-message-submit').click();
    }
};

document.querySelector('#chat-message-submit').onclick = function (e) {
    e.preventDefault()

    const messageInputDom = document.querySelector('#chat-message-input');
    const message = messageInputDom.value;

    console.log({
        'message': message,
        'username': userName,
        'room': roomName,
    })

    if (message) {
        chatSocket.send(JSON.stringify({
            'message': message,
            'username': userName,
            'room': roomName
        }));
    }
    /*window.location.reload();*/
    messageInputDom.value = '';

    return false
};

/**
 * A function for finding the messages element, and scroll to the bottom of it.
 */
function scrollToBottom() {
    let objDiv = document.getElementById("chat-messages");
    objDiv.scrollTop = objDiv.scrollHeight;
}

// Add this below the function to trigger the scroll on load.
scrollToBottom();