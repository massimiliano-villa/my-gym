from django.urls import path
from .views import delete_comment, update_comment
app_name = 'comment'

urlpatterns = [
    path('comment/<int:pk>/comment-update/', update_comment, name='comment_update'),
    path('comment/<int:pk>/delete/', delete_comment, name='comment_delete'),
]
