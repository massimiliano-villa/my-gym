from django.http import HttpResponseForbidden
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, HttpResponseRedirect, reverse, get_object_or_404
from .forms import CommentForm
from .models import Comment


@login_required(login_url="user_auth:login")
def update_comment(request, pk):
    comment = get_object_or_404(Comment, pk=pk)
    if request.user.id == comment.commenter.id or request.user.is_superuser:
        if request.method == 'POST':
            form = CommentForm(request.POST, instance=comment)
            if form.is_valid():
                data = form.cleaned_data
                comment.content = data.get('content')
                comment.save()
                messages.success(request, "Messaggio modificato correttamente!")
                return HttpResponseRedirect(reverse("blog:post_detail", kwargs={'pk': comment.post.id}))
        else:
            form = CommentForm(instance=comment)
            return render(request, "comment/comment_update.html", {"comment_form": form})
    else:
        return HttpResponseForbidden(render(request, "403.html"))


@login_required(login_url="user_auth:login")
def delete_comment(request, pk):
    comment = Comment.objects.filter(pk=pk).first()
    if request.user.is_superuser or request.user.id == comment.commenter.id:
        comment.delete()
        messages.success(request, "Messaggio rimosso con successo!")
        return HttpResponseRedirect(reverse("blog:post_detail", kwargs={'pk': comment.post.id}))
    else:
        return HttpResponseForbidden(render(request, "403.html"))
