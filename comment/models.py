from django.db import models
from blog.models import BlogPost
from gym_user.models import GymUser


class Comment(models.Model):
    post = models.ForeignKey(BlogPost, on_delete=models.CASCADE, related_name='comments')
    commenter = models.ForeignKey(GymUser, on_delete=models.CASCADE)
    content = models.TextField()
    date = models.DateField(auto_now=True)

    def __str__(self):
        return self.commenter.username + " - " + self.post.title + " - " + self.date.__str__()

    class Meta:
        verbose_name_plural = 'Commenti'
