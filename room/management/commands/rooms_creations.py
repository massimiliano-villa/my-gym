from room.models import Room
from django.core.management.base import BaseCommand


class Command(BaseCommand):

    def handle(self, *args, **options):
        self.stdout.write(self.style.SUCCESS('Caricamento le stanze nel db...'))

        if not Room.objects.exists():
            rooms = {
                "name": ["corsi", "off-topic", "novità"],
                "slug": ["course-slg", "off-topic-slug", "news-slug"],
            }
            for i in range(3):
                rm = Room()
                for k in rooms:
                    if k == "name":
                        rm.name = rooms[k][i]
                    if k == "slug":
                        rm.slug = rooms[k][i]
                rm.save()
        else:
            self.stderr.write('Stanze già caricate, cancello l\'operazione.')
            return

        self.stdout.write(self.style.SUCCESS('Stanze caricate correttamente'))

