from django.urls import path
from .views import chat, rooms

app_name = 'room'

urlpatterns = [
    path('', rooms, name='chat-rooms'),
    path('<slug:slug>/chat/', chat, name='chat'),
]
