from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import render

from .models import Room, Message


@login_required(login_url='user_auth:login')
def rooms(request):
    all_rooms = Room.objects.all()
    return render(request, 'room/rooms.html', {'rooms': all_rooms})


@login_required(login_url='user_auth:login')
def chat(request, slug):
    det_room = Room.objects.get(slug=slug)
    messages = Message.objects.filter(room=det_room)
    return render(request, "room/chat.html", {'room': det_room, 'chat_mess': messages})
