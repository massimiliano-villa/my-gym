from django.apps import AppConfig


class GymCourseConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'gym_course'
