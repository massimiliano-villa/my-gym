from django.urls import path
from .views import add_to_queue, courses_list, make_inscription, mark_as_read, unsubscribe
app_name = 'gym_course'

urlpatterns = [
    path('', courses_list, name='courses'),
    path('iscriviti/<int:course_pk>/', make_inscription, name='make_inscription'),
    path('mettiti-in-lista/<int:course_pk>/', add_to_queue, name='add_to_queue'),
    path('discriviti/<int:course_pk>/<str:to_queue>/', unsubscribe, name='unsubscribe'),
    path('mark-as-read/<int:not_slug>/', mark_as_read, name='mark_not_as_read'),
]
