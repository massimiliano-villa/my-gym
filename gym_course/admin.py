from django.contrib import admin
from .models import GymCourse, WaitingQueue

admin.site.register(GymCourse)
admin.site.register(WaitingQueue)
