import timerthread
from django.core.management.base import BaseCommand
from django.utils import timezone
from django.utils.datetime_safe import datetime
from gym_course.models import GymCourse, WaitingQueue
from notifications.signals import notify
from notifications.models import Notification

START_OF_COURSES = datetime(timezone.now().year,
                            timezone.now().month,
                            timezone.now().day).date()
TOT_SUBS = 0
COURSE_CAPACITY = 4  # Quantità di utenti che il corso può contenere
COURSE_DURATION = 30  # 30 giorni

# Ogni 10 secondi controlla se ci sono posti liberi per iscriversi ai corsi
CHECK_RECUR_TIME = 10  # 10 secondi


def convert_to_sec(days) -> float:
    return days * 24 * 60 * 60


@timerthread.task('delay', convert_to_sec(COURSE_DURATION))
def delete_courses():

    # Stop thread delle notifiche
    send_not_timer.cancel()

    print("Elimino corsi, code d'attesa, notifiche...")

    GymCourse.objects.all().delete()
    WaitingQueue.objects.all().delete()
    Notification.objects.all().delete()


@timerthread.task('recur', CHECK_RECUR_TIME)
def send_notifications():

    # print("Inizio controllo...")
    if GymCourse.objects.exists():
        for crs in GymCourse.objects.all():
            # print(f"Capacità corso di {crs}: {crs.capacity}")
            # print(f"Corso di {crs} ha {crs.tot_sub} iscritti e
            # {crs.queue.queue_subs.count()} utenti in lista d'attesa")
            # print("-------------------------------")
            if crs.queue.queue_subs.count() > 0 and crs.capacity > 0:
                """
                Invita tutti gli utenti della coda ad iscriversi al corso
                """
                tot_usr = crs.queue.queue_subs.all()
                for usr in tot_usr:
                    just_send = usr.notifications.unread().filter(verb__contains=crs.title)
                    if not just_send and usr not in crs.subs.all():
                        notify.send(sender=usr, recipient=usr,
                                    verb='Il corso di {corso}, ha posti disponibili!'.format(
                                        corso=crs.queue.name.capitalize()), course_id=crs.id)


# Thread per eliminazione programmatica di corsi, code d'attesa e notifiche
delete_course_timer = delete_courses.sched()

# Thread per invio di notifiche a utenti in lista d'attesa
send_not_timer = send_notifications.sched()


class Command(BaseCommand):

    def handle(self, *args, **options):
        self.stdout.write(self.style.SUCCESS('Caricamento corsi nel db...'))

        if not GymCourse.objects.exists():
            courses = {
                "title": ["boxe", "karate", "danza", "calisthenics"],
                "desc": ["Descrizione di boxe", "Descrizione di karate", "Descrizione di danza",
                         "Descrizione di calisthenics"],
            }
            for i in range(4):
                cr = GymCourse()
                for k in courses:
                    if k == "title":
                        cr.title = courses[k][i]
                    if k == "desc":
                        cr.description = courses[k][i]
                cr.date_of_start = START_OF_COURSES
                cr.tot_sub = TOT_SUBS
                cr.capacity = COURSE_CAPACITY
                wq = WaitingQueue()
                wq.name = cr.title
                wq.save()
                cr.queue = wq
                cr.save()
        else:
            self.stderr.write('Corsi gia caricati, cancello l\'operazione.')
            return
        self.stdout.write(self.style.SUCCESS('Corsi caricati correttamente'))

        delete_course_timer.start()
        send_not_timer.start()
