from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib import messages
from notifications.models import Notification
from notifications.utils import slug2id
from .models import GymCourse
from gym_user.models import GymUser


def courses_list(request):
    crs = GymCourse.objects.all()
    return render(request, "gym_course/courses.html", {"courses": crs})


@login_required(login_url="user_auth:login")
def make_inscription(request, course_pk):
    if request.method == 'GET':
        crs = get_object_or_404(GymCourse, pk=course_pk)
        usr = get_object_or_404(GymUser, pk=request.user.pk)
        if usr in crs.subs.all():
            messages.warning(request,
                             "Sei già iscritto/a al corso di {corso}, entra nella tua area personale per vedere i "
                             "dettagli!".format(corso=crs.title.capitalize()))

        elif usr not in crs.subs.all() and crs.capacity > 0:
            if usr in crs.queue.queue_subs.all():
                crs.queue.queue_subs.remove(usr)
                messages.success(request,
                                 "Sei stato/a iscritto/a al corso di {corso}, e discritto alla sua coda d'attesa "
                                 "entra nella tua area personale per vedere i "
                                 "dettagli!".format(corso=crs.title.capitalize()))
            else:
                messages.success(request,
                                 "Sei stato/a iscritto/a al corso di {corso}, entra nella tua area personale per "
                                 "vedere i "
                                 "dettagli!".format(corso=crs.title.capitalize()))
            crs.subs.add(usr)
            crs.tot_sub += 1
            crs.capacity -= 1
            crs.save()

    return redirect("gym_course:courses")


@login_required(login_url="user_auth:login")
def add_to_queue(request, course_pk):
    if request.method == 'GET':
        crs = get_object_or_404(GymCourse, pk=course_pk)
        usr = get_object_or_404(GymUser, pk=request.user.pk)
        if usr in crs.subs.all():
            messages.warning(request,
                             "Sei gia iscritto/a al corso di {corso}, entra nella tua pagina personale per vedere i "
                             "dettagli!".format(corso=crs.title.capitalize()))
        elif usr not in crs.subs.all():
            crs.queue.queue_subs.add(usr)
            crs.save()
            messages.success(request,
                             "Sei stato/a inserito/a nella lista d'attesa del corso di {corso}, "
                             "riceverai un messaggio quando si libererà un posto!".format(corso=crs.title.capitalize()))

    return redirect("gym_course:courses")


@login_required(login_url="user_auth:login")
def unsubscribe(request, course_pk, to_queue):
    if request.method == 'POST':
        crs = get_object_or_404(GymCourse, pk=course_pk)
        usr = get_object_or_404(GymUser, pk=request.user.pk)

        if to_queue == "False":
            crs.subs.remove(usr)
            crs.tot_sub -= 1
            crs.capacity += 1
            crs.save()
            messages.success(request,
                             "Sei stato/a disiscritto dal corso di {corso}!".format(corso=crs.title.capitalize()))
        else:
            crs.queue.queue_subs.remove(usr)
            crs.save()
            messages.success(request, "Sei stato/a disiscritto dalla lista di attesa del corso di {corso}!".format(
                corso=crs.title.capitalize()))

    return redirect("gym_user:profile")


@login_required(login_url="user_auth:login")
def mark_as_read(request, not_slug):
    if request.method == 'GET':
        # utility che converte lo slug dell' entry notifica nel suo id
        not_id = slug2id(not_slug)
        noti = get_object_or_404(Notification, recipient=request.user, id=not_id)

        noti.mark_as_read()  # leggo la notifica
        noti.delete()  # elimino la notifica
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
