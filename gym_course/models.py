from django.db import models
from gym_user.models import GymUser


class WaitingQueue(models.Model):
    name = models.CharField(max_length=50)
    queue_subs = models.ManyToManyField(GymUser, related_name="usr_queue", blank=True)

    def __str__(self):
        return "Lista d'attesa di " + self.name

    def get_tot_subs(self):
        return self.queue_subs.objects.all().count()

    class Meta:
        verbose_name_plural = 'Liste di attesa'


class GymCourse(models.Model):
    title = models.CharField(max_length=50)
    description = models.TextField()
    subs = models.ManyToManyField(GymUser, related_name="subscribers", blank=True)
    queue = models.OneToOneField(WaitingQueue, related_name="course_queue", on_delete=models.CASCADE)
    date_of_start = models.DateField()
    tot_sub = models.PositiveSmallIntegerField()
    capacity = models.PositiveSmallIntegerField()

    def __str__(self):
        return "Corso di " + self.title

    class Meta:
        ordering = ('title',)
        verbose_name_plural = 'Corsi'
