#!/bin/bash
export DJANGO_SUPERUSER_USERNAME="admin"
export DJANGO_SUPERUSER_FIRST_NAME="Massimiliano"
export DJANGO_SUPERUSER_LAST_NAME="Villa"
export DJANGO_SUPERUSER_EMAIL="admin@gmail.com"
export DJANGO_SUPERUSER_PASSWORD="root"
export DJANGO_SUPERUSER_ADDRESS="Giuseppe Verdi 17"
export DJANGO_SUPERUSER_CITY="Modena"
python3 manage.py makemigrations
python3 manage.py migrate
python3 manage.py add_admin
python3 manage.py rooms_creations
python3 manage.py courses_management & 
python3 manage.py runserver
