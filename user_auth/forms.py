from django.contrib.auth.forms import UserCreationForm
from django.forms import ModelForm
from django.utils.translation import gettext_lazy as _
from gym_user.models import GymUser


class CreateUserForm(UserCreationForm):
    class Meta:
        model = GymUser
        fields = ['username', 'first_name', 'last_name', 'email', 'profile_image', 'address', 'city']
        labels = {
            'username': _('Username'),
            'first_name': _('Nome'),
            'last_name': _('Cognome'),
            'profile_image': _('Immagine di profilo'),
            'address': _('Indirizzo'),
            'city': _('Città'),
        }
        help_texts = {
            'username': _('Solo lettere, cifre e @/./+/-/_')
        }


class ChangeUserDataForm(ModelForm):
    class Meta:
        model = GymUser
        fields = ['first_name', 'last_name', 'email', 'profile_image', 'address', 'city']
        exclude = ['password']
        labels = {
            'first_name': _('Nome'),
            'last_name': _('Cognome'),
            'profile_image': _('Immagine di profilo'),
            'address': _('Indirizzo'),
            'city': _('Città'),
        }


class DeleteUserForm(ModelForm):
    class Meta:
        model = GymUser
        fields = []
