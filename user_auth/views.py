from django.contrib import messages
from django.contrib.auth import authenticate, login, logout, update_session_auth_hash
from django.shortcuts import render, HttpResponseRedirect, reverse

from .forms import CreateUserForm, ChangeUserDataForm, DeleteUserForm
from django.contrib.auth.forms import AuthenticationForm, PasswordChangeForm
from django.contrib.auth.decorators import login_required


def register(request):
    templ = 'user_auth/register.html'

    if request.method == 'POST':
        form = CreateUserForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            messages.success(request, 'Utente creato correttamente!')
            return HttpResponseRedirect(reverse("user_auth:login"))
    else:
        form = CreateUserForm()

    return render(request, templ, {"form": form})


def login_user(request):
    templ = 'user_auth/login.html'

    if request.method == 'GET':
        form = AuthenticationForm()
        return render(request, templ, {"form": form})

    if request.method == 'POST':
        form = AuthenticationForm(request=request, data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(request,
                                username=username,
                                password=password)
            if user is not None:
                login(request, user)
                if user.is_superuser:
                    messages.success(request, 'Benvenuto/a Admin!')
                else:
                    messages.success(request, f'Benvenuto/a {user.last_name.capitalize()} \
                    {user.first_name.capitalize()}!')
                return HttpResponseRedirect(reverse("gym_user:profile"))
        else:
            return render(request, 'user_auth/login.html', {"form": form})


def logout_user(request):
    logout(request)
    return HttpResponseRedirect(reverse("home"))


@login_required(login_url="user_auth:login")
def change_password(request):
    if request.method == "POST":
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Importante !
            messages.success(request, "La tua password è stata aggiornata correttamente!")
            return HttpResponseRedirect(reverse("user_auth:login"))
    else:
        form = PasswordChangeForm(request.user)
    return render(request, "user_auth/change_password.html", {"form": form})


@login_required(login_url="user_auth:login")
def change_user_data(request):
    if request.method == "POST":
        form = ChangeUserDataForm(request.POST, request.FILES, instance=request.user)
        if form.is_valid():
            form.save()
            messages.success(request, 'Profilo aggiornato correttamente!')
            return HttpResponseRedirect(reverse("gym_user:profile"))
    else:
        form = ChangeUserDataForm(instance=request.user)

    return render(request, "user_auth/change_user_data.html", {"form": form})


@login_required(login_url="user_auth:login")
def delete_user(request):
    if request.method == "POST":
        form = DeleteUserForm(request.POST, instance=request.user)
        if form.is_valid():
            user = request.user
            user.delete()
            messages.success(request, "Profilo eliminato correttamente!")
            return HttpResponseRedirect(reverse("home"))
    else:
        form = DeleteUserForm(instance=request.user)

    return render(request, 'user_auth/delete_user.html', {"delete_form": form})
