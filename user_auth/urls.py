from django.urls import path
from .views import change_password, change_user_data, delete_user, login_user, logout_user, register
app_name = 'user_auth'

urlpatterns = [
    path('login/', login_user, name='login'),
    path('logout/', logout_user, name='logout'),
    path('register/', register, name='register'),
    path('change_password/', change_password, name='change_password'),
    path('change_user_data/', change_user_data, name='change_data'),
    path('delete_user/', delete_user, name='user_delete'),
]
