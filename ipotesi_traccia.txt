Applicazione Web per la gestione di sistema di prenotazioni di corsi in una palestra.
L’applicazione è utilizzabile sia da utenti anonimi che da utenti registrati:
• gli utenti anonimi possono navigare i contenuti del sito, tra cui una pagina che illustra le
attrezzature della palestra , la pagina con le informazioni dei corsi e una pagina gestita
dall’amministratore del sito nella quale potrà pubblicare articoli di natura alimentare e affini a un
allenamento corretto
• gli utenti registrati sulla piattaforma potranno iscriversi a uno o più corsi proposti e commentare
gli articoli creati dall’amministratore
L’utente registrato e successivamente autenticato, avrà a disposizione una pagina personale nella quale
saranno presenti i suoi dati personali, una foto e le eventuali iscrizioni a corsi.
Tramite la sua pagina personale, l’utente potrà modificare i suoi dati personali oppure gestire le sue
eventuali iscrizioni a corsi.
L’amministratore del sito può creare, modificare o eliminare un articolo.
Gli articoli sono contrassegnati da una data di creazione, una foto, il testo dell’articolo, una categoria
strettamente legata alla natura dell’articolo.
L’utente può visualizzare tutti gli articoli postati dall’amministratore in sequenza o raggruppati per categoria.

aggiungerei:
- lista di attesa per iscrizione a corsi già pieni - se un utente cancella la prenotazione per una certa lezione chi è in lista di attesa riceve una notifica (ragioni su come gestire questa cosa)
- sistema di messaggistica tra utenti / clienti della palestra e il gestore della palestra (che immagino sia quello che chiama amministratore)

Inoltre, l'amministratore dovrà avere interfacce per il caricamento dei suoi articoli (che non lo faccia da adim)
