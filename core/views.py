from django.views.generic import TemplateView


class Home(TemplateView):
    template_name = 'my-gym/home.html'


class Gym(TemplateView):
    template_name = 'my-gym/gym.html'


class Services(TemplateView):
    template_name = 'my-gym/services.html'


class Contacts(TemplateView):
    template_name = 'my-gym/contacts.html'

