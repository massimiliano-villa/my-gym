import notifications.urls
from django.contrib import admin
from django.urls import include, path, re_path
from core.views import Home, Contacts, Gym, Services
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    re_path(r'^$', Home.as_view(), name='home'),
    path('palestra/', Gym.as_view(), name='gym'),
    path('servizi/', Services.as_view(), name='services'),
    path('corsi/', include('gym_course.urls')),
    path('contattaci/', Contacts.as_view(), name='contacts'),
    path('blog/', include('blog.urls')),
    path('blog/blogpost/', include('comment.urls')),
    path('user/', include('gym_user.urls')),
    path('rooms/', include('room.urls')),
    path('auth/', include('user_auth.urls')),
    path('inbox/notifications/', include(notifications.urls, namespace='notifications')),
]

"""
By doing this, Django development server can able to serve the media
files during development mode when setting  DEBUG = TRUE  in settings.py file.
"""
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
