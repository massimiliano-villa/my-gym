from django.forms import ModelForm
from blog.models import BlogPost, Category
from django.utils.translation import gettext_lazy as _
from django import forms


class BlogPostCreateForm(ModelForm):
    class Meta:
        model = BlogPost
        fields = ('title', 'blog_img', 'content', 'category')
        labels = {
            'title': _('Titolo'),
            'blog_img': _('Immagine del post'),
            'content': _('Contenuto'),
            'category': _('Categoria')
        }
