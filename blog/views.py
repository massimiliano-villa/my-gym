from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect, render
from django.contrib import messages
from django.urls import reverse_lazy, reverse
from django.views.generic import CreateView, DeleteView, ListView, UpdateView
from blog.forms import BlogPostCreateForm
from blog.models import BlogPost, Category
from django.contrib.auth.decorators import permission_required
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin

from comment.forms import CommentForm


class CategoryCreate(PermissionRequiredMixin, SuccessMessageMixin, CreateView):
    model = Category
    fields = "__all__"
    template_name = "category/category_creation.html"
    success_url = reverse_lazy("gym_user:profile")

    permission_required = "blog.add_category"
    raise_exception = True
    success_message = "Categoria creata correttamente!"


class CategoryDelete(PermissionRequiredMixin, SuccessMessageMixin, DeleteView):
    model = Category
    template_name = "category/category_delete.html"
    success_url = reverse_lazy("gym_user:profile")

    permission_required = "blog.delete_category"
    raise_exception = True
    success_message = "Categoria eliminata correttamente!"


class CategoryList(PermissionRequiredMixin, ListView):
    model = Category
    template_name = "category/category_list.html"
    context_object_name = "categorylist"

    permission_required = "blog.view_category"
    raise_exception = True


@permission_required("blog.add_blogpost", raise_exception=True)
def blog_post_create(request):
    if request.method == "POST":
        form = BlogPostCreateForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            messages.success(request, "Post creato correttamente!")
            return redirect("gym_user:profile")
    else:
        form = BlogPostCreateForm()

    ctx = {"warning": "Crea almeno una categoria prima di inserire un post",
           "categories": Category.objects.all(),
           "tot_cat": Category.objects.count(),
           "form": form}

    temp = "blog/post_creation.html"
    return render(request, temp, ctx)


class BlogPostDelete(PermissionRequiredMixin, SuccessMessageMixin, DeleteView):
    model = BlogPost
    template_name = "blog/post_delete.html"
    success_url = reverse_lazy("gym_user:profile")

    permission_required = "blog.delete_blogpost"
    raise_exception = True
    success_message = "Post eliminato correttamente!"


class BlogPostUpdate(PermissionRequiredMixin, UpdateView):
    model = BlogPost
    form_class = BlogPostCreateForm
    template_name = "blog/post_update.html"

    permission_required = "blog.change_blogpost"
    raise_exception = True

    def get_success_url(self):
        pk = self.get_context_data()["object"].pk
        messages.success(self.request, 'Post aggiornato correttamente!')
        return reverse_lazy("gym_user:profile")


def blog_post_detail(request, pk):
    blog_post = get_object_or_404(BlogPost, pk=pk)
    temp = "blog/post_detail.html"

    ctx = {"post": blog_post,
           "categories": Category.objects.all(),
           "last_articles": BlogPost.objects.all().order_by('-date')[:5]}

    if request.user.is_authenticated:
        if request.method == "POST":
            form = CommentForm(request.POST)
            if form.is_valid():
                comment = form.save(commit=False)
                comment.post = blog_post
                comment.commenter = request.user
                comment.save()
                messages.success(request, "Messaggio postato correttamente!")
                return HttpResponseRedirect(reverse("blog:post_detail", kwargs={'pk': pk}))
        else:
            form = CommentForm()

        ctx.update({"comment_form": form})

        return render(request, temp, ctx)

    return render(request, temp, ctx)


def blog_post_list(request):
    blogposts = BlogPost.objects.all()
    categories = Category.objects.all()
    tot_posts = BlogPost.objects.all().count()
    return render(request,
                  "blog/posts_list.html",
                  {"blogposts": blogposts,
                   "categories": categories,
                   "tot_post": tot_posts})


def filtered_posts(request, name):
    if name == "all":
        posts = BlogPost.objects.all()
    else:
        posts = BlogPost.objects.filter(category__name__contains=name)
    return render(request, "blog/filtered_posts.html", {"blogposts": posts})
