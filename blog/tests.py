import datetime
from django.test import TestCase
from django.urls import reverse

from gym_user.models import GymUser
from blog.models import Category, BlogPost


def create_test_user(username, password, email, usr_is_admin):
    usr = GymUser.objects.create_user(username=username, password=password, email=email)

    if usr_is_admin == "False":
        usr.is_staff = False,
        usr.is_superuser = False
    else:
        usr.is_staff = True
        usr.is_superuser = True

    usr.is_active = True

    usr.save()
    return usr


def create_test_category(pk, name):
    return Category.objects.create(pk=pk, name=name)


def create_test_blogpost(pk, title, blog_img, content, date):
    return BlogPost.objects.create(
        pk=pk,
        title=title,
        blog_img=blog_img,
        content=content,
        date=date)


class BlogPostTest(TestCase):

    def setUp(self):
        self.simple_usr = create_test_user('username', 'password', '256091@studenti.unimore.it', False)
        self.admin_usr = create_test_user('admin', 'root', 'admin@gmail.com', True)

        self.simple_usr.save()
        self.admin_usr.save()

        self.blogpost = create_test_blogpost(1,
                                             'Allenamento ottimale',
                                             'blog_img',
                                             'Lorem ipsum lorem ipsum',
                                             datetime.date(2022, 12, 1))

        self.cat = create_test_category(1, 'fitness')
        self.blogpost.category.add(self.cat)

        self.blogpost2 = create_test_blogpost(2,
                                              'Allenamento ideale',
                                              'blog_img',
                                              'Lorem ipsum lorem ipsum',
                                              datetime.date(2022, 12, 1))

        self.cat2 = create_test_category(2, 'wellness')
        self.blogpost2.category.add(self.cat2)

    def test_post_creation_forbidden_request_if_not_authenticated(self):
        response = self.client.get(reverse('blog:post_creation'))
        self.assertEqual(str(response.context['user']), 'AnonymousUser')
        self.assertEqual(response.status_code, 403)
        self.assertTemplateUsed(response, '403.html')
        self.assertTemplateNotUsed(response, 'blog/post_creation.html')

    def test_category_creation_forbidden_if_not_authenticated(self):
        response = self.client.get(reverse('blog:category_creation'))
        self.assertEqual(str(response.context['user']), 'AnonymousUser')
        self.assertEqual(response.status_code, 403)
        self.assertTemplateUsed(response, '403.html')
        self.assertTemplateNotUsed(response, 'category/category_creation.html')

    def test_post_creation_allowed_request_if_is_superuser(self):
        self.client.login(username='admin', password='root')
        response = self.client.get(reverse('blog:post_creation'))
        self.assertEqual(str(response.context['user']), 'admin')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateNotUsed(response, '403.html')
        self.assertTemplateUsed(response, 'blog/post_creation.html')

    def test_category_creation_allowed_request_if_is_superuser(self):
        self.client.login(username='admin', password='root')
        response = self.client.get(reverse('blog:category_creation'))
        self.assertEqual(str(response.context['user']), 'admin')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateNotUsed(response, '403.html')
        self.assertTemplateUsed(response, 'category/category_creation.html')

    def test_post_modification_forbidden_request_if_is_not_authenticated(self):
        response = self.client.get(reverse('blog:post_update', args=[1]))
        self.assertEqual(str(response.context['user']), 'AnonymousUser')
        self.assertEqual(response.status_code, 403)
        self.assertTemplateUsed(response, '403.html')
        self.assertTemplateNotUsed(response, 'blog/post_update.html')

    def test_post_modification_allowed_request_if_is_superuser(self):
        self.client.login(username='admin', password='root')
        response = self.client.get(reverse('blog:post_update', args=[1]))
        self.assertEqual(str(response.context['user']), 'admin')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'blog/post_update.html')
        self.assertTemplateNotUsed(response, '403.html')

    def test_post_elimination_forbidden_request_if_is_not_authenticated(self):
        response = self.client.get(reverse('blog:post_delete', args=[1]))
        self.assertEqual(str(response.context['user']), 'AnonymousUser')
        self.assertEqual(response.status_code, 403)
        self.assertTemplateUsed(response, '403.html')
        self.assertTemplateNotUsed(response, 'blog/post_delete.html')

    def test_post_elimination_allowed_request_if_is_superuser(self):
        self.client.login(username='admin', password='root')
        response = self.client.get(reverse('blog:post_delete', args=[1]))
        self.assertEqual(str(response.context['user']), 'admin')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'blog/post_delete.html')
        self.assertTemplateNotUsed(response, '403.html')

    def test_category_elimination_forbidden_request_if_is_not_authenticated(self):
        response = self.client.get(reverse('blog:category_delete', args=[1]))
        self.assertEqual(str(response.context['user']), 'AnonymousUser')
        self.assertEqual(response.status_code, 403)
        self.assertTemplateUsed(response, '403.html')
        self.assertTemplateNotUsed(response, 'category/category_delete.html')

    def test_category_elimination_allowed_request_if_is_superuser(self):
        self.client.login(username='admin', password='root')
        response = self.client.get(reverse('blog:category_delete', args=[1]))
        self.assertEqual(str(response.context['user']), 'admin')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'category/category_delete.html')
        self.assertTemplateNotUsed(response, '403.html')

    def test_post_detail_allowed_request_if_is_not_authenticated(self):
        response = self.client.get(reverse('blog:post_detail', args=[1]))
        self.assertEqual(str(response.context['user']), 'AnonymousUser')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'blog/post_detail.html')
        self.assertTemplateNotUsed(response, '403.html')

    def test_post_detail_allowed_request_if_is_superuser(self):
        self.client.login(username='admin', password='root')
        response = self.client.get(reverse('blog:post_detail', args=[1]))
        self.assertEqual(str(response.context['user']), 'admin')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'blog/post_detail.html')
        self.assertTemplateNotUsed(response, '403.html')

    def test_filtered_posts_per_category_request(self):
        response = self.client.get(reverse('blog:filter', args=["fitness"]))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'blog/filtered_posts.html')
        self.assertQuerysetEqual(response.context['blogposts'], ['<BlogPost: Allenamento ottimale>'])

        response = self.client.get(reverse('blog:filter', args=["wellness"]))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'blog/filtered_posts.html')
        self.assertQuerysetEqual(response.context['blogposts'], ['<BlogPost: Allenamento ideale>'])
