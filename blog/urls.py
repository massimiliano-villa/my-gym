from django.urls import path
from .views import blog_post_create, blog_post_detail, BlogPostDelete, blog_post_list, BlogPostUpdate, filtered_posts
from .views import CategoryCreate, CategoryDelete, CategoryList

app_name = 'blog'

urlpatterns = [
    path('post-list/', blog_post_list, name='posts_list'),
    path('blogpost/create/', blog_post_create, name='post_creation'),
    path('blogpost/<int:pk>/delete/', BlogPostDelete.as_view(), name='post_delete'),
    path('blogpost/<int:pk>/detail/', blog_post_detail, name='post_detail'),
    path('blogpost/<int:pk>/update/', BlogPostUpdate.as_view(), name='post_update'),
    path('category/create/', CategoryCreate.as_view(), name='category_creation'),
    path('category/<int:pk>/delete/', CategoryDelete.as_view(), name='category_delete'),
    path('category-list/', CategoryList.as_view(), name='category_list'),
    path('find/<str:name>/', filtered_posts, name='filter')
]
