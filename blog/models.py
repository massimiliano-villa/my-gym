from django.db import models


class Category(models.Model):

    name = models.CharField(max_length=30, blank=False, unique=True, verbose_name="Nome")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Categorie'


class BlogPost(models.Model):

    title = models.CharField(max_length=100)
    blog_img = models.ImageField(upload_to='blog-img-upload')
    content = models.TextField()
    date = models.DateField(auto_now=True)
    category = models.ManyToManyField(Category)

    def __str__(self):
        return self.title

    @property
    def image_url(self):
        if self.blog_img and hasattr(self.blog_img, 'url'):
            return self.blog_img.url

    class Meta:
        ordering = ('-date',)
        verbose_name_plural = 'Posts'
