import os
from gym_user.models import GymUser
from django.core.management import BaseCommand, call_command


class Command(BaseCommand):
    def handle(self, *args, **options):
        try:
            GymUser.objects.get(username=os.getenv("DJANGO_SUPERUSER_USERNAME"))
            self.stderr.write(self.style.ERROR('Admin gia creato!'))
            return
        except GymUser.DoesNotExist:
            self.stdout.write(self.style.SUCCESS('Creo l\'admin: l\'amministratore della palestra...'))
            call_command('createsuperuser', interactive=False)
            self.stdout.write(self.style.SUCCESS('Admin creato correttamente!'))
