from django.db import models
from django.contrib.auth.models import AbstractUser, User


class GymUser(AbstractUser):

    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    email = models.EmailField()
    profile_image = models.ImageField(upload_to='user-pic-upload/',
                                      blank=True)

    address = models.CharField(max_length=25, blank=True)
    city = models.CharField(max_length=25, blank=True)

    REQUIRED_FIELDS = ["first_name", "last_name",
                       "email", "address",
                       "city"]

    def __str__(self):
        return self.username

    @property
    def image_url(self):
        if self.profile_image and hasattr(self.profile_image, 'url'):
            return self.profile_image.url
