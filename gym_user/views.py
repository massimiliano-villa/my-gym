from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404
from blog.models import BlogPost, Category
from gym_user.models import GymUser


@login_required(login_url='user_auth:login')
def profile_view(request):
    templ = 'gym_user/profile.html'

    if request.user.is_superuser:
        posts = BlogPost.objects.all()
        cat = Category.objects.all()
        return render(request, templ, {"posts": posts, "cate": cat})
    else:
        return render(request, templ)


@login_required(login_url='user_auth:login')
def load_notification(request):
    if request.method == 'GET':
        usr = get_object_or_404(GymUser, pk=request.user.pk)
        return render(request, "my-gym/notifications.html", {"noti": usr.notifications.unread()})
