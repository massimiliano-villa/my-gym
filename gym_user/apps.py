from django.apps import AppConfig


class GymUserConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'gym_user'
