from django.urls import path
from .views import load_notification, profile_view
app_name = 'gym_user'

urlpatterns = [
    path('profile/', profile_view, name='profile'),
    path('load_notification', load_notification, name='load_notification'),

]
