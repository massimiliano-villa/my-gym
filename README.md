# MY-GYM

## Libraries

```
Pipenv, version 2022.1.8
Django v4.0.6 (https://docs.djangoproject.com/en/4.0/)
Bootstrap v5.1.3 (https://getbootstrap.com/docs/5.1/getting-started/introduction/)
JQuery v3.6.0 (https://jquery.com/)

```
## Preparing the environment

```
git clone https://gitlab.com/massimiliano-villa/my-gym.git
cd my-gym

pipenv install -r requirements.txt && pipenv shell

export DJANGO_SUPERUSER_USERNAME="admin"
export DJANGO_SUPERUSER_FIRST_NAME="Massimiliano"
export DJANGO_SUPERUSER_LAST_NAME="Villa"
export DJANGO_SUPERUSER_EMAIL="admin@gmail.com"
export DJANGO_SUPERUSER_PASSWORD="root"
export DJANGO_SUPERUSER_ADDRESS="Giuseppe Verdi 17"
export DJANGO_SUPERUSER_CITY="Modena"

```

## Apply and create migrations

```
python3 manage.py makemigrations
python3 manage.py migrate

```

## Create admin

```
python3 manage.py add_admin

```

## Create rooms

```
python3 manage.py rooms_creations
```

## Create courser

```
python3 manage.py courses_management &
```

## Run project

```
python3 manage.py runserver
```

## All in one

```
cd my-gym
./start_project.sh
```
## Do test

```
python3 manage.py test blog
```
